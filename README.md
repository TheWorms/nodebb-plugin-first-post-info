![nodebb compatibility](https://packages.nodebb.org/api/v1/plugins/nodebb-plugin-first-post-info/compatibility.png)

# NodeBB Plugin First Post Info

This plugins gives the option to display a dialog with some custom information to a user posting his first time or opening his first topic.

After installing and activating the plugin, head to your ACP and configure the plugin under `Plugins -> First Post Info`. There you can configure if the info should be displayed for the first post or first topic only. Keep in mind that creating a topic is also creating a post.

It is possible to use the forums markup language in the message which allows it to be formatted nicely. At the moment only one message is supported for all users independent of their language settings. The message thus can't be translated into other languages.
