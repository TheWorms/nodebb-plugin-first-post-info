"use strict";

/* globals window, $, app, socket, bootbox */

(function () {
    $(window).on('action:composer.topic.new', function() {
        if(app.user.topiccount !== 0) {
            return;
        }

        socket.emit('plugins.firstPostInfo.getInfoHtml', {}, function (err, result) {
            if(err || !result || !result.displayMessage || result.displayMessage.trim().length === 0) {
                return;
            }

            bootbox.dialog({
                message: result.displayMessage,
                closeButton: false,
                buttons: {
                    ok: {
                        label: "[[nodebb-plugin-first-post-info:message_close]]",
                        class: "btn btn-primary"
                    }
                }
            });
        });
    });
})();
