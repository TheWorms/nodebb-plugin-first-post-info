'use strict';

/* globals define, $, app, socket */

define('admin/plugins/first-post-info', ['settings', 'translate'], function (Settings, Translator) {

    const settingKey = 'nodebb-plugin-first-post-info';
    const settingHtmlId = '.first-post-info-settings';

    let FirstPostInfo = {};

    FirstPostInfo.init = function () {
        Settings.load(settingKey, $(settingHtmlId), function (err, settings) {
            if (err) {
                settings = {};
            }

            const defaults = {
                message: '',
                displayOnFirstPost: false,
                displayOnFirstTopic: true,
            };

            // Set defaults
            for (let setting in defaults) {
                if (!settings.hasOwnProperty(setting)) {
                    if (typeof defaults[setting] === 'boolean') {
                        $('#' + setting).prop('checked', defaults[setting]);
                    } else {
                        $('#' + setting).val(defaults[setting]);
                    }
                }
            }
        });

        $('#save').on('click', function () {
            Settings.save(settingKey, $(settingHtmlId), function () {
                app.alert({
                    type: 'success',
                    alert_id: 'nodebb-plugn-first-post-info-settings-saved',
                    title: '[[nodebb-plugin-first-post-info:settings_saved_title]]',
                    message: '[[nodebb-plugin-first-post-info:settings_saved_message]]',
                });
            });
        });

        $('#previewMessage').on('click', function (evt) {
            evt.preventDefault();
            let params = {
                displayMessage: $('#displayMessage').val(),
                displayPreview: true,
            };

            socket.emit('plugins.firstPostInfo.getInfoHtml', params, function (err, result) {
                if(err || !result || !result.displayMessage || result.displayMessage.trim().length === 0) {
                    app.alert({
                        type: 'error',
                        alert_id: 'nodebb-plugn-first-post-info-settings-preview',
                        title: '[[nodebb-plugin-first-post-info:settings_preview_failed_title]]',
                        message: '[[nodebb-plugin-first-post-info:settings_preview_failed_message]]',
                    });
                    return;
                }

                bootbox.dialog({
                    message: result.displayMessage,
                    closeButton: false,
                    buttons: {
                        ok: {
                            label: "[[nodebb-plugin-first-post-info:message_close]]",
                            class: "btn btn-primary"
                        }
                    }
                });
            });
        });
    };

    return FirstPostInfo;
});
