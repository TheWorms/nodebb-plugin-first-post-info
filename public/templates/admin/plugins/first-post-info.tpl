<div class="row">
    <div class="col-sm-2 col-xs-12 settings-header">[[nodebb-plugin-first-post-info:settings_general_section_header]]</div>
    <div class="col-sm-10 col-xs-12">
        <form class="form first-post-info-settings">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">[[nodebb-plugin-first-post-info:settings_display_header]]</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="displayOnFirstTopic">
                                        <input type="checkbox" name="displayOnFirstTopic" id="displayOnFirstTopic" />
                                        [[nodebb-plugin-first-post-info:settings_display_first_topic]]
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="displayOnFirstPost">
                                        <input type="checkbox" name="displayOnFirstPost" id="displayOnFirstPost" />
                                        [[nodebb-plugin-first-post-info:settings_display_first_post]]
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">[[nodebb-plugin-first-post-info:settings_display_message]]</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <textarea class="form-control" rows="10" name="displayMessage" id="displayMessage"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <button id="previewMessage" class="btn btn-default">[[nodebb-plugin-first-post-info:settings_message_preview]]</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<button id="save" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
    <i class="material-icons">[[nodebb-plugin-first-post-info:settings_save]]</i>
</button>
